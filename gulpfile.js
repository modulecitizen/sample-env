// Require Gulp

'use strict';


var gulp = require('gulp');
var plugin = require('./gulp/helpers/plugins');
var browserSync = require('browser-sync').create();


gulp.task('sass', function() {
    return gulp.src('scss/**/*.scss')
      .pipe(plugin.sass())
      .pipe(gulp.dest('css'))
      .pipe(browserSync.stream());
});

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

        gulp.watch('**/*.scss', ['sass']);

});