'use strict';

var gulp = require('gulp'),
    plugin = require('../helpers/plugins'),
    pipe = require('../pipes');

/**
 * Builds a build-task.
 *
 * @param {Glob} source
 * @param {Function} pipes - returns the array of task-specific pipes
 * @param {Glob} target
 *
 * @returns {task}
 */

module.exports = function buildTask(source, pipes, target) {
    return function(cb) {
        gulp.src(source)
            .pipe(pipes())
            .pipe(gulp.dest(target))
        cb()
    };
};
